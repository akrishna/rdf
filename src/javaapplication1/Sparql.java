/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import arq.query;
import com.hp.hpl.jena.graph.query.Query;
import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import de.congrace.exp4j.Calculable;
import de.congrace.exp4j.ExpressionBuilder;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author krishna
 */
public class Sparql {
                
                public HashMap m1;
                private Model model;
                public Sparql() {

                String inputFileName="king/ankur.rdf";
                InputStream in = null;
                    try 
                    {
                        in= new FileInputStream(inputFileName);
                    }   
                    catch (FileNotFoundException ex) 
                    {

                        Logger.getLogger(Sparql.class.getName()).log(Level.SEVERE, null, ex);
                    }
            // Create an empty in-memory model and populate it from the graph
                 model = ModelFactory.createDefaultModel();
                 model.read(in,null); 
            //create a hashmap for the values to be used in calculations  
                 m1 = new HashMap();
                 m1.put("Temperature_data_C", (double)25);
                 m1.put("Windspeed_data_mPs", (double)50);
                 
                 
     
    }
    
             //fetch out all the functions that can be used in evaluating physical_concept   
   Vector get_functions_list(String physical_concept)
                {
                    Vector formula=new Vector();
                    String solution[]=new String[3];
                    int j=0;
                    String queryString ="PREFIX base: <http://www.semanticweb.org/ontologies/2011/7/S&A.owl#>"+
                                        "SELECT ?functions " +
                                        "WHERE {" +
                                        "     base:"+physical_concept+" base:provided_by ?functions . " +

                                        "     }";

                    com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString);

                    // Execute the query and obtain results
                    QueryExecution qe = QueryExecutionFactory.create(query, model);
                    ResultSet results = qe.execSelect();
                    for ( ; results.hasNext() ; )
                    {
                    QuerySolution soln = results.next() ;
                    //conver to proper formula string
                    solution[j]=soln.toString();
                    int i=solution[j].indexOf('#');
                    int k=solution[j].indexOf('>');
                    solution[j]=solution[j].substring(i+1,k);
                    formula.add(solution[j]);
                    j++;
                    }



                    return formula;
   
                }
   //fetch out the mathematical expression of each concerned function
   Vector formula(String physical_concept)
   {
            
                 Vector formula=new Vector();
                 String solution[]=new String[3];
                 int j=0;
                 // Create a new query to find out all the graph models relatives of windchill_factor individual
                
                 String queryString ="PREFIX base: <http://www.semanticweb.org/ontologies/2011/7/S&A.owl#>"+
                                    "SELECT ?formula " +
                                    "WHERE {" +
                                    "     base:"+physical_concept+" base:provided_by ?name . " +
                                    "     ?name  base:mathematical_expression ?formula "+
                                    "     }";

                 com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString);

                // Execute the query and obtain results
                 QueryExecution qe = QueryExecutionFactory.create(query, model);
                 ResultSet results = qe.execSelect();
                 for ( ; results.hasNext() ; )
                {
                  QuerySolution soln = results.next() ;
                  //conver to proper formula string
                  solution[j]=soln.toString();
                  int i=solution[j].indexOf('=');
                  solution[j]=solution[j].substring(i+1);
                  i=solution[j].indexOf('=');
                  solution[j]=solution[j].substring(i+1);
                  i=solution[j].indexOf('"');
                  solution[j]=solution[j].substring(0,i);
                  solution[j]=this.parse(solution[j]);
                  formula.add(solution[j]);
                  j++;
                }

                
               
                return formula;
   
    }
    //fetch out list of inputs for a specific formula
    Vector input_values(String formula)
    {           Vector v = new Vector() ;
                String queryString ="PREFIX base: <http://www.semanticweb.org/ontologies/2011/7/S&A.owl#>"+
                                            "SELECT ?input " +
                                            "WHERE {" +
                                            "    base:"+formula+" base:has_input ?input  . " +
                                           /* "   OPTIONAL { base:"+formula+" base:has_input_a_value ?input  }" + 
                                            "   OPTIONAL { base:"+formula+" base:symbol ?input  }" + */
                                            "      }";
                 
                  com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString);

                // Execute the query and obtain results
                  QueryExecution qe = QueryExecutionFactory.create(query, model);
                  ResultSet results = qe.execSelect();
                // ResultSetFormatter.out(System.out, results, query);
                 for ( ; results.hasNext() ; )
                {
                  QuerySolution soln = results.next() ;
                  //conver to proper formula string
                  String solution=soln.toString();
                  int i=solution.indexOf('#');
                  solution=solution.substring(i+1);
                  i=solution.lastIndexOf('>');
                  solution=solution.substring(0,i);
                  v.add(solution);
                  
                  
                }

                
               
                return v;
    }
    //fetch out the symbols of the function inputs
    Vector get_symbol(Vector input)
    {               
                
                
                Vector v = new Vector() ;
               
                int i=0;
                for(i=0;i<input.size();i++)
                {
                String queryString ="PREFIX base: <http://www.semanticweb.org/ontologies/2011/7/S&A.owl#>"+
                                            "SELECT ?symbol " +
                                            "WHERE {" +
                                            "    base:"+input.elementAt(i)+" base:symbol ?symbol . " +
                                            "      }";
                com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString);

                        // Execute the query and obtain results
                        QueryExecution qe = QueryExecutionFactory.create(query, model);
                        ResultSet results = qe.execSelect();
                       // ResultSetFormatter.out(System.out, results, query);
                        for ( ; results.hasNext() ; )
                        {
                        QuerySolution soln = results.next() ;
                        //conver to proper formula string
                        String solution=soln.toString();
                        int k=solution.indexOf('"');
                        int j=solution.lastIndexOf('"');
                        solution=solution.substring(k+1,j);
                        v.add(solution);


                      }
                        
                }      
        return v;
    }
    //fetch out the concept linked with the function parameters
    Vector get_physical_concept(Vector input)
    {               
                
                
                Vector v = new Vector() ;
               
                int i=0;
                for(i=0;i<input.size();i++)
                {
                String queryString ="PREFIX base: <http://www.semanticweb.org/ontologies/2011/7/S&A.owl#>"+
                                            "SELECT ?concept " +
                                            "WHERE {" +
                                            "    base:"+input.elementAt(i)+" base:physical_concept ?concept . " +
                                            "      }";
                com.hp.hpl.jena.query.Query query = QueryFactory.create(queryString);

                        // Execute the query and obtain results
                        QueryExecution qe = QueryExecutionFactory.create(query, model);
                        ResultSet results = qe.execSelect();
                        //ResultSetFormatter.out(System.out, results, query);
                        for ( ; results.hasNext() ; )
                        {
                        QuerySolution soln = results.next() ;
                        //conver to proper formula string
                        String solution=soln.toString();
                        int k=solution.indexOf('#');
                        int j=solution.lastIndexOf('>');
                        solution=solution.substring(k+1,j);
                        v.add(solution);


                      }
                        
                }      
        return v;
    }
    
    //retun position of nth occourance of a character in a string
    int n_indexof(String formula,char c,int pos){
        
                int position;
                position= formula.indexOf(c,0);
                while (pos--> 1 && position != -1)
                {
                    position = formula.indexOf(c, position+1);
                    //System.out.println(position);
                    
                }
                return position;
        
        
    }
    
    //convert the string formula into a standard form to be given as input in mathematical parser.
    String parse(String formula)
    {           
        
                String std_formula ="" ;
                int count = formula.length() - formula.replace("[", "").length();
                //System.out.println(count);
                n_indexof(formula,'[',3);
                for(int i=1;i<=count;i++)
               {
                      int value1=n_indexof(formula,'[',i);
                      int value2=formula.indexOf(']',value1+1);
                      //System.out.println(value1+"---"+value2);
                      String sub_string=formula.substring(value1+1,value2);
                      std_formula=std_formula+sub_string;
                      //System.out.println(sub_string);
               }
                return std_formula;
    }
    //retun a string having detailed information of the physical_concept
    String exact_string(String physical_concept)
    {
                Vector functions=new Vector();
                functions=this.get_functions_list(physical_concept);
                Vector parameter[]=new Vector[functions.size()];
                Vector concept[]=new Vector[functions.size()];
                Vector symbol[]=new Vector[functions.size()];
                for(int i=0;i<functions.size();i++)
                {
                    parameter[i]=this.input_values(functions.elementAt(i).toString());
                    
                }
                for(int i=0;i<functions.size();i++)
                {
                    concept[i]=this.get_physical_concept(parameter[i]);
                    symbol[i]=this.get_symbol(parameter[i]);
                    
                }
                
                return "<<< PC  "+functions.elementAt(0).toString()+","+parameter[0].elementAt(0).toString()+","+concept[0].elementAt(0).toString()+
                        ","+symbol[0].elementAt(0).toString()+","+parameter[0].elementAt(1).toString()+","+concept[0].elementAt(1).toString()+
                        ","+symbol[0].elementAt(1).toString()+
                        ","+functions.elementAt(1).toString()+","+parameter[1].elementAt(0).toString()+","+concept[1].elementAt(0).toString()+
                        ","+symbol[1].elementAt(0).toString()+","+parameter[1].elementAt(1).toString()+","+concept[1].elementAt(1).toString()+
                        ","+symbol[1].elementAt(1).toString()+"  >>>"
                        
                        ;
    }
    //return calculated value of a physical concept,use constants from Hashmap.
     double computeConceptValue(String physical_concept) throws UnknownFunctionException, UnparsableExpressionException
    {
               
               
               Vector formula=this.formula(physical_concept);
               double result[]=new double[formula.size()];
               Vector functions=this.get_functions_list(physical_concept);
               double temp=(double) m1.get("Temperature_data_C");
               double Wind_speed=(double) m1.get("Windspeed_data_mPs");
               //System.out.println(temp+"  "+Wind_speed);
               for(int i=0;i<formula.size();i++)
               {
               
               ExpressionBuilder builder=new ExpressionBuilder(formula.elementAt(i).toString()).withVariable("W",Wind_speed).withVariable("Ta",temp);
               Calculable calc=builder.build();
               result[i]= calc.calculate();
               System.out.println(functions.elementAt(i).toString()+"->"+result[i]);
               
               }
               
               return (result[0]+result[1])/2;
               
               
    }

 
    
}

