/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import de.congrace.exp4j.Calculable;
import de.congrace.exp4j.ExpressionBuilder;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;

/**
 *
 * @author krishna
 */
public class ext_parser {
    public static void main(String args[]) throws UnknownFunctionException, UnparsableExpressionException {
    ExpressionBuilder builder=new ExpressionBuilder("2 * 17.41 + (12*2)^(0-1)");
    Calculable calc=builder.build();
    double result = calc.calculate();
    System.out.println(result);
    }
}
