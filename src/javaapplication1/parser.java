/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author krishna
 */
import java.util.Stack;
/**
 * Class to evaluate infix and postfix expressions.
 * 
 */
public class parser {

        /**
         * Operators in reverse order of precedence.
         */
        private static final String operators = "-+/*e";
        private static final String operands = "0123456789.";

        public String evalInfix(String infix) {
                return (convert2Postfix(infix));
        }

        public String convert2Postfix(String infixExpr) {
                char[] chars = infixExpr.toCharArray();
                Stack stack=new Stack();
                StringBuilder out = new StringBuilder(infixExpr.length());

                for (char c : chars) {
                        if (isOperator(c)) {
                                if(stack.isEmpty())
                                {
                                    out.append(" "); 
                                }
                                while (!stack.isEmpty() && stack.peek() != '(') {
                                        if (operatorGreaterOrEqual((char)stack.peek(), c)) {
                                            out.append(" ");    
                                            out.append(stack.pop());
                                            
                                        } else {
                                            
                                                break;
                                        }
                                }
                                stack.push(c);
                        } else if (c == '(') {
                                stack.push(c);
                        } else if (c == ')') {
                                while (!stack.isEmpty() && stack.peek() != '(') {
                                           
                                        out.append(stack.pop());
                                         
                                }
                                if (!stack.isEmpty()) {
                                        stack.pop();
                                }
                        } else if (isOperand(c)) {
                                   
                                out.append(c); 
                        }
                }
                while (!stack.empty()) {
                           
                        out.append(stack.pop());
                         
                }
                return out.toString();
        }

        public int evaluatePostfix(String postfixExpr) {
                char[] chars = postfixExpr.toCharArray();
                Stack stack=new Stack();
                for (char c : chars) {
                        if (isOperand(c)) {
                                stack.push(c - '0'); // convert char to int val
                        } else if (isOperator(c)) {
                                int op1 = (int) stack.pop();
                                int op2 = (int) stack.pop();
                                int result;
                                switch (c) {
                                case '*':
                                        result = op1 * op2;
                                        stack.push(result);
                                        break;
                                case '/':
                                        result = op2 / op1;
                                        stack.push(result);
                                        break;
                                case '+':
                                        result = op1 + op2;
                                        stack.push(result);
                                        break;
                                case '-':
                                        result = op2 - op1;
                                        stack.push(result);
                                        break;
                                case 'e':
                                        result =(int) Math.pow(op2,op1);
                                        stack.push(result);
                                        break;
                                }
                        }
                }
                return (int) stack.pop();
        }
        private int getPrecedence(char operator) {
                int     ret=0;
                if(operator=='e')
                {
                        ret=3;
                }
                if (operator == '-' || operator == '+') {
                        ret=1;
                } else if (operator == '*' || operator == '/')  {
                        ret=2;
                }
                return ret;
        }
        private boolean operatorGreaterOrEqual(char op1, char op2) {
                return getPrecedence(op1)>= getPrecedence(op2);
        }

        private boolean isOperator(char val) {
                return operators.indexOf(val)>= 0;
        }

        private boolean isOperand(char val) {
                return operands.indexOf(val)>= 0;
        }

}