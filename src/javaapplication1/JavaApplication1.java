package javaapplication1;


import com.hp.hpl.jena.rdf.model.*;

/**
 * A small family tree held in a Jena Model
 */
public class JavaApplication1 {
  
  // Namespace declarations
  static final String familyUri = "http://localhost/family#";
  static final String relationshipUri = "http://localhost/relationship#";

  // Jena model declaraton
  private Model model;

  /*
  below constructor will design the rdf model and will put all concerned values 
  * and properties
   */
  private JavaApplication1() {
    
    // Create an empty Model
    model = ModelFactory.createDefaultModel();
    
    // Create the types of Property we need to describe relationships
    // in the model
    Property childOf = model.createProperty(relationshipUri,"childOf");
    Property parentOf = model.createProperty(relationshipUri,"parentOf");
    Property siblingOf = model.createProperty(relationshipUri,"siblingOf");
    Property spouseOf = model.createProperty(relationshipUri,"spouseOf");
    Property grandparentOf = model.createProperty(relationshipUri,"grandparentOf");
    Property grandsonOf = model.createProperty(relationshipUri,"grandsonOf");
    Property husbandOf = model.createProperty(relationshipUri,"husbandOf");
    // Create resources representing the people in our model
    Resource grandfather = model.createResource(familyUri+"grandfather");
    Resource grandmother = model.createResource(familyUri+"grandmother");
    Resource father= model.createResource(familyUri+"father");
    Resource mother = model.createResource(familyUri+"mother");
    Resource a = model.createResource(familyUri+"a");
    Resource b = model.createResource(familyUri+"b");
  
    // Add properties to describing the relationships between them
    grandfather.addProperty(grandparentOf,a);
    grandfather.addProperty(grandparentOf,b);
    grandfather.addProperty(parentOf,father);
    grandfather.addProperty(husbandOf,grandmother);
    
    
    grandmother.addProperty(grandparentOf,a);
    grandmother.addProperty(grandparentOf,b);
    grandmother.addProperty(spouseOf,grandfather);
    grandmother.addProperty(parentOf,father);
    
  
    father.addProperty(husbandOf,mother);
    father.addProperty(parentOf,a);
    father.addProperty(parentOf,b);
    father.addProperty(childOf,grandmother);
    father.addProperty(childOf,grandfather);
    
    mother.addProperty(spouseOf,father);
    mother.addProperty(parentOf,a);
    mother.addProperty(parentOf,b);
   
    //now to add a statement directly
    // Arrays of Statements can also be added to a Model:
    Statement statements[] = new Statement[10];
    statements[0] = model.createStatement(a,childOf,father);
    statements[1] = model.createStatement(a,childOf,mother);
    statements[2] = model.createStatement(a,siblingOf,b);
    statements[3] = model.createStatement(a,grandsonOf,grandfather);
    statements[4] = model.createStatement(a,grandsonOf,grandmother);
    
    statements[5] = model.createStatement(b,childOf,father);
    statements[6] = model.createStatement(b,childOf,mother);
    statements[7] = model.createStatement(b,siblingOf,a);
    statements[8] = model.createStatement(b,grandsonOf,grandfather);
    statements[9] = model.createStatement(b,grandsonOf,grandmother);
    
    model.add(statements);

  }
    
  void write_model()
  {
      model.write(System.out);
  }
  void turtle_form()
  {
      StmtIterator iter = model.listStatements();

    // print out the predicate, subject and object of each statement
    while (iter.hasNext()) {
    Statement stmt      = iter.nextStatement();  // get next statement
    Resource  subject   = stmt.getSubject();     // get the subject(relation from)
    Property  predicate = stmt.getPredicate();   // get the predicate(property)
    RDFNode   object    = stmt.getObject();      // get the object(value/relation)
    System.out.print(subject.toString());
    System.out.print(" " + predicate.toString() + " ");
    System.out.print(" "+object.toString()+" ");
    System.out.println(" .");
  }
  
  }  
  /**
   * Creates a FamilyModel and dumps the content of its RDF representation
   */
  public static void main(String args[]) {

    // Create a model representing the family
    JavaApplication1 theFamily = new JavaApplication1();

   //to write model
   theFamily.write_model();
   
   //to view subject predicate object form
   theFamily.turtle_form();
   //
   
   
  }
}
